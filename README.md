# Examples:

* aws-asg-slowscale.cli        - .Net Core CLI Example
* aws-asg-slwoscale.console    - .Net 4.6.2 CLI Example
* aws-asg-slowscale.Example    - .Net 4.6.2, written as a unit test

# Code:

* aws-asg-slowscale            - .Net Class Lib that is what you can call
* aws-asg-slowscale.Model      - .Net class with model

#Usage:

##Simple / Default:
```cs
// Scale to 2, defaults (every 60 seconds increasing by 1)
ScaleUp su = new ScaleUp();
su.Scaleup(new Model.ScaleChange("Example", 2));
```

##Detailed Instructions:
```cs
// Scale to 120, every 30 seconds increasing by 5
ScaleUp su = new ScaleUp();
su.Scaleup(new Model.ScaleChange("Example", 120, 30, 5));
```

