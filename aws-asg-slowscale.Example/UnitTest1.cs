﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace aws_asg_slowscale.Example
{
    [TestClass]
    public class ExampleAsUnitTest
    {
        [TestMethod]
        public void ScaleExample_Simple()
        {
            ScaleUp su = new ScaleUp();
            su.Scaleup(new Model.ScaleChange("Example", 2));                // Scale to 2, defaults (every 60 seconds increasing by 1)
        }

        [TestMethod]
        public void ScaleExample_Detailed()
        {
            ScaleUp su = new ScaleUp();
            su.Scaleup(new Model.ScaleChange("Example", 120, 30, 5));       // Scale to 120, every 30 seconds increasing by 5
        }
    }
}
