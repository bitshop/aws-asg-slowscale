﻿using System;

namespace aws_asg_slowscale.Model
{
    public class ScaleChange
    {
        public String ASGName;
        public int NewDesired;
        public int SecondsBetweenChanges;
        public int CountEachChange;

        public ScaleChange()
        {
            NewDesired = 0;
            SecondsBetweenChanges = 60000;
            CountEachChange = 1;
        }

        public ScaleChange(string ASGName, int NewDesired, int SecondsBetweenChanges = 60, int CountEachChange = 1)
        {
            this.SecondsBetweenChanges = SecondsBetweenChanges;
            this.NewDesired = NewDesired;
            this.CountEachChange = CountEachChange;
            this.ASGName = ASGName;
        }
    }
}
