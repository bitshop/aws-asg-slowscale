﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aws_asg_slowscale.console
{
    class Program
    {
        static void Main(string[] args)
        {
            Model.ScaleChange scalechange = CheckArgs(args);
            if (scalechange != null)
            {
                Console.WriteLine(String.Format("ASG Name: {0}", scalechange.ASGName));
                aws_asg_slowscale.ScaleUp scaleup = new ScaleUp();
                scaleup.Scaleup(scalechange);
            }
        }

        static Model.ScaleChange CheckArgs(string[] args)
        {
            Boolean success = false;
            String ASGName = "";
            int NewDesired = 0;
            int SecondsBetweenChanges = 60;
            int CountEachChange = 1;

            if (args.Length < 2 || !int.TryParse(args[1], out NewDesired))
                success = false;
            else
            {
                ASGName = args[0];
                if (int.TryParse(args[1], out NewDesired))
                {
                    switch (args.Length)
                    {
                        case 2:
                            success = true;
                            break;
                        case 3:
                            if (int.TryParse(args[2], out SecondsBetweenChanges))
                                success = true;
                            break;
                        case 4:
                            if (int.TryParse(args[2], out SecondsBetweenChanges))
                                if (int.TryParse(args[3], out CountEachChange))
                                    success = true;
                            break;
                        default:
                            success = false;
                            break;
                    }
                }
            }
            if (!success)
            {
                WriteHelp();
                return null;
            }
            else
            {
                Model.ScaleChange scalechange = new Model.ScaleChange(ASGName, NewDesired, SecondsBetweenChanges, CountEachChange);
                return (scalechange);
            }
        }

        static void WriteHelp()
        {
            Console.WriteLine("aws-asg-slowscale <ASGName> <NewDesiredCapacity>");
            Console.WriteLine("aws-asg-slowscale <ASGName> <NewDesiredCapacity> <SecondsBetweenChanges>");
            Console.WriteLine("aws-asg-slowscale <ASGName> <NewDesiredCapacity> <SecondsBetweenChanges> <CountEachChange>");
        }
    }
}
