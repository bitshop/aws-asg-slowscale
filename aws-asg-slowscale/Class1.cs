﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.AutoScaling;
using Amazon.AutoScaling.Model;

namespace aws_asg_slowscale
{
    public class ScaleUp
    {

        #region PublicMethods
        public Boolean Scaleup(Model.ScaleChange scalechange)
        {
            try
            {
                int CurrentRunning;
                Task<int> t = GetASGDesired(scalechange.ASGName);
                t.Wait();
                CurrentRunning = t.Result;
                Console.WriteLine(
                    String.Format("Result was we had [{0}] running", CurrentRunning));
                while (CurrentRunning < scalechange.NewDesired)
                {
                    Task<int> t2 = SetASGDesired(scalechange.ASGName, CurrentRunning + Math.Min(scalechange.CountEachChange, (scalechange.NewDesired - CurrentRunning)));
                    t2.Wait();
                    CurrentRunning = t2.Result;
                    Console.WriteLine(
                        String.Format("Result was we had [{0}] desired", CurrentRunning));
                    Console.WriteLine("Waiting ....");
                    System.Threading.Thread.Sleep(1000 * scalechange.SecondsBetweenChanges);
                }
                return (true);
            }
            catch (Exception e)
            {
                // The things that can go wrong include:
                //      AWS Security not defined
                //      AWS Security: Access Denied
                //      ASG doesn't exist (in GetASGDesired: Exception("Didn't find the ASG"))
                // How should we gracefully return these? 
                //      For this iteration we will just throw the AWS exception
                throw e;
            }
        }
        #endregion PublicMethods

        #region InternalMethods
        async Task<int> GetASGDesired(String ASGName)
        {
            Amazon.AutoScaling.AmazonAutoScalingClient awsclient =
                new AmazonAutoScalingClient(Amazon.RegionEndpoint.USEast1);
            var response = await awsclient.DescribeAutoScalingGroupsAsync(new DescribeAutoScalingGroupsRequest
            {
                AutoScalingGroupNames = new List<String>
                {
                    ASGName
                }
            });

            if (response.AutoScalingGroups.Count != 1)
                throw new Exception("Didn't find the ASG");
            else
            {
                return (response.AutoScalingGroups[0].DesiredCapacity);
                /* FYI:
                 *          foreach (AutoScalingGroup asg in response.AutoScalingGroups)
                 *              {
                 *                  Console.WriteLine(String.Format("ASG [ {0} ] - Desired [ {1} ] - Max [ {2} ]",
                 *                      asg.AutoScalingGroupName, asg.DesiredCapacity, asg.MaxSize));
                 *              }
                 *  However we are requesting one specific ASG and we already threw an exception if not [0]
                */
            }
        }

        async Task<int> SetASGDesired(String ASGName, int NewDesired)
        {
            Amazon.AutoScaling.AmazonAutoScalingClient awsclient =
                new AmazonAutoScalingClient(Amazon.RegionEndpoint.USEast1);

            await awsclient.SetDesiredCapacityAsync(new SetDesiredCapacityRequest
            {
                AutoScalingGroupName = ASGName,
                DesiredCapacity = NewDesired,
                HonorCooldown = false
            });
            return (await GetASGDesired(ASGName));
        }
        #endregion InternalMethods
    }
}
